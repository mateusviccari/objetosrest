/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.mviccari.objetosrest.beans;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Pessoa {
    
    private Integer codigo;
    private String nome;
    private BigDecimal altura;
    private Profissao profissao;

    public Pessoa() {
    }

    public Pessoa(Integer codigo, String nome, BigDecimal altura, Profissao profissao) {
        this.codigo = codigo;
        this.nome = nome;
        this.altura = altura;
        this.profissao = profissao;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BigDecimal getAltura() {
        return altura;
    }

    public void setAltura(BigDecimal altura) {
        this.altura = altura;
    }

    public Profissao getProfissao() {
        return profissao;
    }

    public void setProfissao(Profissao profissao) {
        this.profissao = profissao;
    }
    
    
    
}
