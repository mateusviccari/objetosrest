/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.mviccari.objetosrest.rest;

import br.mviccari.objetosrest.beans.Pessoa;
import br.mviccari.objetosrest.beans.Profissao;
import java.math.BigDecimal;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Avell G1711 NEW
 */
@Path("Pessoas")
public class Pessoas {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of Pessoas
     */
    public Pessoas() {
    }
    
    @GET
    @Produces(MediaType.APPLICATION_XML)
    @Path("PegaPessoa")
    public Pessoa pegaPessoa(){
        return new Pessoa(10, "Mateus Viccari", new BigDecimal("182.0"), new Profissao(27, "Programador"));
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_XML)
    @Consumes(MediaType.APPLICATION_XML)
    @Path("AlteraPessoa")
    public Pessoa alteraPessoa(Pessoa pessoa){
        pessoa.setNome("Isaura");
        pessoa.setProfissao(new Profissao(25, "Escrava"));
        pessoa.setAltura(new BigDecimal(10.0));
        return pessoa;
    }

}
